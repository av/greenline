# Greenline World (ROS 1)

* [Installation](#installation)
* [Start the World](#start-the-world)
* [Visualize and Control the Turtlebot](#visualize-and-control-the-turtlebot)
  * [Option 1: Manual Control](#option-1-manual-control)
  * [Option 2: Short-range Planning and Navigation](#option-2-short-range-planning-and-navigation)
  * [Option 3: Long-range Planning and Navigation](#option-3-long-range-planning-andnavigation)


___
The Greenline World is a Gazebo world created for this class as a testbed for mapping, planning and navigation including line following and obstacle avoidance.  

![Greenline World](.Images/Greenline_World.png)

# Installation

The Turtlebot packages must be installed first, see `AV Notes / Setup / ROS_Packages`.  Then clone this Greenline repo into your `~/catkin_ws/src` folder.  You will need to source the overlay for each command using the Greenline world, since it is in your catkin workspace.

# Start the World

Simply run:
```
$ roslaunch greenline greenline.launch
```
# Visualize and Control the Turtlebot

There are three options for visualizing and controlling the robot.  The first is simple manual control, while the second runs a mapper and short-range planning, and the third uses the long-range and short-range planners.  Choose one option:

## Option 1: Manual Control

To see the Lidar data and load the Turtlebot model into Rviz, you can run:
```
$ roslaunch greenline rviz.launch
```
To control the robot you can use the teleop package:
```
$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```
Or you can publish the `/cmd_vel` topic with:
```
$ rostopic pub /cmd_vel <tab> <tab>
```
Here `<tab> <tab>` means press the tab key twice to complete the command and fill in linear x speed and angular z components.

## Option 2: Short-range Planning and Navigation
In order to send waypoint commands to the robot, it is necessary to publish a `map` along with a map coordinate system transformation.  Additionally the `move_base` package needs to be run (as this does the short-range planning).  These can all be launched with:
```
$ roslaunch greenline mapping.launch
```
With `move_base` package running, you can use the `reset.py` utility to command the robot to move to pre-programmed waypoints specified by the ellipses:
```
$ rosrun greenline reset.py
```
This will let you command the robot to drive to any of the 6 waypoints.  Enter a number outside of this range to quit.

Mapping will create an occupancy map of the environment. This can be saved by running:
```
$ rosrun map_server map_saver -f ~/map
```
This will output two files: `~/map.yaml` and `~/map.pgm`.  

## Option 3: Long-range Planning and Navigation

The long-range planner is `amcl`, see [http://wiki.ros.org/amcl](http://wiki.ros.org/amcl), which works in conjunction with the short-range planner `move_base`.  These are integrated in the `turtlebot3_navigation` package.  To run this you will first need to build and save a map of your environment, see Option 2 above.  Then quit `mapping.launch` and start `turtlebot3_navigation` with:
```
$ roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml
```

Instructions on running the planner are here: [https://emanual.robotis.com/docs/en/platform/turtlebot3/navigation/#estimate-initial-pose](https://emanual.robotis.com/docs/en/platform/turtlebot3/navigation/#estimate-initial-pose).  It involves initializing the pose of the Turtlebot using the `2D Pose Estimate` button in RViz to align the robot with the map, and then setting a goal with `2D Nav Goal`.

![Navigation](.Images/Greenline_navigation.png)


