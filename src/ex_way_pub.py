#!/usr/bin/env python
'''
  Publish waypoints in a PoseArray to topic: /waypoints
'''
__author__ = 'Daniel Morris'
import rospy
import math

from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler


class waypoints_pub():

    def __init__(self):

        self.way_pub = rospy.Publisher('waypoints', PoseArray, latch=True, queue_size=1)

        #Create a set of waypoint poses
        #Here they are hard-coded, but you will generate your own from sensed objects

        wplist = PoseArray()

        pnt1 = Point( -1., 1.6, 0.)
        quat1 = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
        wplist.poses.append(Pose(pnt1,quat1))
        pnt2 = Point( 0.5, 1.1, 0.)
        quat2 = Quaternion(*(quaternion_from_euler(0, 0, -90.*math.pi/180, axes='sxyz')))
        wplist.poses.append(Pose(pnt2,quat2))
        pnt3 = Point( 0., -0.4, 0.)
        quat3 = Quaternion(*(quaternion_from_euler(0, 0, -90*math.pi/180, axes='sxyz')))
        wplist.poses.append(Pose(pnt3,quat3))
        pnt4 = Point( 0., -1.6, 0.)
        quat4 = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
        wplist.poses.append(Pose(pnt4,quat4))
        pnt5 = Point( 1.5, -1.6, 0.)
        quat5 = Quaternion(*(quaternion_from_euler(0, 0, 90.*math.pi/180, axes='sxyz')))
        wplist.poses.append(Pose(pnt5,quat5))

        self.way_pub.publish(wplist)
        rospy.loginfo('Publishing '+str(len(wplist.poses))+' waypoints')

if __name__ == '__main__':

    rospy.init_node('waypoints_creator')

    try:
        waypoints_pub()
    except rospy.ROSInterruptException:
        rospy.logerr("Interrupted")

    rospy.spin()
