#!/usr/bin/env python
''' transform_frames.py

    Class to transform point and coordinates between robot frames

    Daniel Morris, April 2020
    Copyright 2020
'''
import rospy
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray, PoseStamped
from std_msgs.msg import Header
import tf2_ros, tf2_geometry_msgs

class transform_frames():
    def __init__(self):
        ''' Create a buffer of transforms, and start TransformListener which updates the buffer in the background '''
        self.tfBuffer = tf2_ros.Buffer()                    # Creates a frame buffer
        listen = tf2_ros.TransformListener(self.tfBuffer)   # Tells TransformListener to fill the buffer
    
    def get_transform(self, from_frame, to_frame):
        ''' Find the transform between one frame and another frame -- looks up the latest transform from the buffer '''
        try:
            trans = self.tfBuffer.lookup_transform(to_frame, from_frame, rospy.Time(0), rospy.Duration(0.2) )
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.logerr('Error, cannot find transformation from %s to %s' % (from_frame, to_frame) )
        return trans     # Will raise an error if no transform is found -- perhaps there is a better way than crashing

    def pose_transform(self, pose_array, from_frame='base_footprint', to_frame='odom'):
        ''' pose_array: contains an array of points or poses in from_frame, and returns a PoseArray with 
            the same points or poses in to_frame
            If it cannot find the transform between frames the returns empty PoseArray
        '''
        trans = self.get_transform( from_frame, to_frame )
        pose_array_transformed = PoseArray()  # Transformed points
        header = Header(frame_id=from_frame, stamp=rospy.Time(0)) # Want our array to have latest time
        for pose in pose_array.poses:
            pose_s = PoseStamped(pose=pose, header=header)
            pose_t = tf2_geometry_msgs.do_transform_pose(pose_s, trans)  #transforms PoseStamed
                     #Note, could also run: tf2_geometry_msgs.do_transform_point() -- transforms PointStamped
            pose_array_transformed.poses.append( pose_t.pose )
        return pose_array_transformed

    def get_frame_A_origin_frame_B(self, frame_A, frame_B ):
        ''' Returns the pose of the origin of frame_A in frame_B as a PoseStamped '''
        header = Header(frame_id=frame_A, stamp=rospy.Time(0))        
        origin_A = Pose(position=Point(0.,0.,0.), orientation=Quaternion(0.,0.,0.,1.))
        origin_A_stamped = PoseStamped( pose=origin_A, header=header ) #Coorinates are set at origin and zero rotation
        pose_frame_B = tf2_geometry_msgs.do_transform_pose(origin_A_stamped, self.get_transform(frame_A, frame_B))
        return pose_frame_B

