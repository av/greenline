#!/usr/bin/env python
''' Reset robot to fixed set of waypoints
    Before running this, run:    
     roslaunch greenline turtlebot3_greenline.launch
     roslaunch greenline start_movebase.launch
'''
import rospy
import argparse
import smach
import rospkg
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler
from std_msgs.msg import Int32
from transform_frames import transform_frames

nav_frame = 'map'

class penalties:
    def __init__(self):
        self.total = 0
        self.pub = rospy.Publisher("/penalties", Int32, latch=True, queue_size=1)
        self.pub.publish( self.total )
    def add(self, np=2):
        self.total -= np        
        self.pub.publish( self.total )
        rospy.loginfo('Total penalty score: '+str(self.total))

class waypoints:
    def __init__(self, isfinal):
        self.trans = transform_frames()
        self.isfinal = isfinal

    def get_line_waypoints(self, x, y, yaw): 
        wplist = PoseArray()
        pnt3 = Point(x, y, 0.)
        quat3 = Quaternion(*(quaternion_from_euler(0, 0, yaw, axes='sxyz')))
        wplist.poses.append(Pose(pnt3, quat3))
        wplist_map = self.trans.pose_transform(wplist, 'odom', nav_frame)
        return wplist_map

def init_follower_sm(wplist):
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded', 'aborted', 'preempted'])
    # These three outcomes are standard for action states, and are the outcomes of SimpleActionState

    with sm:  # This opens sm container for adding states:
        for i, wp in enumerate(wplist.poses):
            wpose = MoveBaseGoal()
            wpose.target_pose.header.frame_id = nav_frame
            wpose.target_pose.header.stamp = rospy.Time.now()
            wpose.target_pose.pose = wp
            name = 'wp' + str(i)
            if i < len(wplist.poses) - 1:
                nextname = 'wp' + str(i + 1)
            else:
                nextname = 'succeeded'
            smach.StateMachine.add(name,
                                   SimpleActionState('move_base',
                                                     MoveBaseAction,
                                                     goal=wpose,
                                                     server_wait_timeout=rospy.Duration(10.0)),
                                   transitions={'succeeded': nextname})
    return sm

class readOdom:
    ''' This class subscribes to /odom and makes it available as self.pose'''

    def __init__(self):
        self.pose = Pose()
        rospy.Subscriber("/odom", Odometry, self.callback)
        rospy.sleep(0.1)  # Make sure pose has time to be initialized with callback
        rospy.loginfo('Subscribing to /odom')

    def callback(self, msg):
        '''Copies odometry pose to self.pose'''
        self.pose = msg.pose.pose

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='reset')
    parser.add_argument('--final', dest='final', action='store_true', help='For final competition')    
    args = parser.parse_args()

    rospack = rospkg.RosPack()
    rospack.get_path('greenline')

    rospy.init_node('runReset', anonymous=True)
    ro = readOdom()
    pen = penalties()
    wp = waypoints(args.final)
    keepgoing = True
    while keepgoing:
        ans = int(input("Choose a spot (1,2,3,4,5,6): "))
        if ans == 1:
            wplist = wp.get_line_waypoints(-1.5, 1.5, 0.)
            pen.add(2)
        elif ans == 2:
            wplist = wp.get_line_waypoints(-0.7, 1.5, -0.1)
            pen.add(2)
        elif ans == 3:
            wplist = wp.get_line_waypoints(0.25, 1.05, -1.7)
            pen.add(2)
        elif ans == 4:
            wplist = wp.get_line_waypoints(0.0, -0.58, -1.6)
            pen.add(2)
        elif ans == 5:
            wplist = wp.get_line_waypoints(0, -1.58, 0.1)
            pen.add(2)
        elif ans == 6:
            wplist = wp.get_line_waypoints(0.78, -1.6, 0.1)
            pen.add(2)
        elif ans == -100:
            wplist = wp.get_line_waypoints(1.5, -0.5, 0.)  #drive into red region
        else:
            print("Not one of the number 1-6, quitting")
            keepgoing = False
            continue
        sm = init_follower_sm(wplist)  # Create state machine
        outcome = sm.execute()  # Execute state machine
        rospy.loginfo('Final outcome: ' + outcome)
